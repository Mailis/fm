# React.js C#   REST API


------------------------------------------

INTRODUCTION:


Table view of computed electricity consumption.

User can insert start and end dates, select a language (ET, EN), insert kW/h price and grouping (day, week, month).

At server side, once requested data is being cached.


------------------------------------------


TECHNOLOGIES:

FrontEnd:

React.js, HTML, CSS3



BackEnd:

.NET, C#, REST API





.