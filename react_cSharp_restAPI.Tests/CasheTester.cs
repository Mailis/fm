﻿using System;
using System.Collections.Generic;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using react_cSharp_restAPI.Caching;
using react_cSharp_restAPI.Parsers;
using react_cSharp_restAPI.Patterns;

namespace react_cSharp_restAPI.Tests
{

    [TestClass]
    public class CasheTester
    {
        private IMemoryCache cache;
        MemoryCacheEntryOptions cacheExpirationOptions;
        int cacheDays = 1;
        

        string start = "01-05-2018";
        string end = "05-06-2018";
        string start_false = "01-14-2018";
        string end_false = "33-06-2018";

        string grp = "week";

        //inject caching interface
        public CasheTester()
        {
            this.cache = new MemoryCache(new MemoryCacheOptions());//cache; // IMemoryCache cache// 
            //set cachexpire options
            cacheExpirationOptions = new MemoryCacheEntryOptions();
            cacheExpirationOptions.AbsoluteExpiration = DateTime.Now.AddDays(cacheDays);
            cacheExpirationOptions.Priority = CacheItemPriority.Normal;
        }


        [TestMethod]
        public void Test_MemoryCache()
        {
            try{
                string casheKey = CacheKeyCreator.getCacheKey(start, end, grp);

                IDictionary<string, InnerGroup> fakeData = new Dictionary<string, InnerGroup>();

                string fakeKey = "fake";
                //test null data
                fakeData.Add(fakeKey, null);

                //cache the data
                cache.Set<string>(casheKey, JsonConvert.SerializeObject(fakeData), cacheExpirationOptions);
                string obj;
                if (cache.TryGetValue(casheKey, out obj))
                {
                    IDictionary<string, InnerGroup> desrerialized = JsonConvert.DeserializeObject<IDictionary<string, InnerGroup>>(obj);
                    var item = desrerialized[fakeKey];
                    Assert.IsNull(item);
                }

                //empty cache
                cache.Remove(fakeData);

                //test data is not null
                fakeData.Remove("fake");
                fakeData.Add("fake", new InnerGroup());
                //cache the data
                cache.Set<string>(casheKey, JsonConvert.SerializeObject(fakeData), cacheExpirationOptions);
                if (cache.TryGetValue(casheKey, out obj))
                {
                    IDictionary<string, InnerGroup> desrerialized = JsonConvert.DeserializeObject<IDictionary<string, InnerGroup>>(obj);
                    Assert.IsNotNull(desrerialized[fakeKey]);
                }

                //empty cache
                cache.Remove(fakeData);
            }
            catch (Exception e){
                string d = e.ToString();
            }
        }
    }
}
