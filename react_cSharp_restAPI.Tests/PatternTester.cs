﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using react_cSharp_restAPI.Models;
using react_cSharp_restAPI.Parsers;
using react_cSharp_restAPI.Patterns;

namespace react_cSharp_restAPI.Tests
{
    [TestClass]
    public class PatternTester
    {
        string start = "01-05-2016";
        string end = "05-06-2018";
        string day_grp = "day";
        string week_grp = "week";
        string month_grp = "month";

        [TestMethod]
        public void Test_Patterns()
        {
            //example: https://finestmedia.ee/kwh/?start=01-05-2018&end=05-06-2018
            //construct request url
            //string url = "https://finestmedia.ee/kwh/?start=" + this.start + "&end=" + this.end;
            string url = @"..\..\..\StaticFiles\XMLFile.xml";
            try
            {
                var xml = XDocument.Load(url);

                // Query the data and write out a subset of ConsumptionHistory
                var q = from history in xml.Root.Descendants("ConsumptionHistory")
                        from element in history.Elements()
                        select new HourConsumptionBeforeParse(element.Attribute("ts").Value, element.Value.Replace(".", ","));

                DateTime startDateTime, endDateTime;
                DateTimeValidator.isValidDate(start, end, out startDateTime, out endDateTime);
                RawDataParser rawParser = new RawDataParser(startDateTime, endDateTime, q);
                //stores parsed data
                List<HourConsumption> hourConsumption = rawParser.parseRawData();
                //test that each date is in beteen start and end
                foreach (HourConsumption h in hourConsumption)
                {
                    Assert.IsTrue(h.date >= startDateTime && h.date <= endDateTime);
                }


                AbstractGroupFactory groupFactory = new ConcreteGroupFactory();

                IGroup concreteGroup = groupFactory.getObject(day_grp);
                Assert.IsInstanceOfType(concreteGroup, typeof(DayGroup));
                //innergroup sums up KWH values and stores dates of a period for a group
                IDictionary<string, InnerGroup> groupedData = concreteGroup.groupData(hourConsumption);
                Assert.IsNotNull(groupedData);
                Assert.IsTrue(groupedData.Count() > 0);


                concreteGroup = groupFactory.getObject(week_grp);
                Assert.IsInstanceOfType(concreteGroup, typeof(WeekGroup));

                //innergroup sums up KWH values and stores dates of a period for a group
                groupedData = concreteGroup.groupData(hourConsumption);
                Assert.IsNotNull(groupedData);
                Assert.IsTrue(groupedData.Count() > 0);


                concreteGroup = groupFactory.getObject(month_grp);
                Assert.IsInstanceOfType(concreteGroup, typeof(MonthGroup));
                
                //innergroup sums up KWH values and stores dates of a period for a group
                groupedData = concreteGroup.groupData(hourConsumption);
                Assert.IsNotNull(groupedData);
                Assert.IsTrue(groupedData.Count() > 0);

            }
            catch (Exception e)
            {
                var d = e.ToString();
                Console.Write(d);
            }

        }
    }
}
