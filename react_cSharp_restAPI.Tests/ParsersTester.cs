﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using react_cSharp_restAPI.Parsers;

namespace react_cSharp_restAPI.Tests
{
    [TestClass]
    public class ParsersTester
    {
        string start = "01-05-2018";
        string end = "05-06-2018";
        string start_false = "01-14-2018";
        string end_false = "33-06-2018";


        [TestMethod]
        public void Test_DateTimeValidator()
        {
            DateTime startDateTime, endDateTime;
            //are date time strings parseble
            bool areValidDates = (DateTimeValidator.isValidDate(this.start, this.end, out startDateTime, out endDateTime));
            Assert.IsTrue(areValidDates);
            areValidDates = (DateTimeValidator.isValidDate(this.start_false, this.end, out startDateTime, out endDateTime));
            Assert.IsFalse(areValidDates);
            areValidDates = (DateTimeValidator.isValidDate(this.start, this.end_false, out startDateTime, out endDateTime));
            Assert.IsFalse(areValidDates);
        }


        [TestMethod]
        public void Test_getDayKey()
        {
            DateTime dt = new DateTime();
            string dayKey = DateGranulator.getDayKey(dt);
            Assert.IsTrue(dayKey.Length == 10);
            Assert.IsFalse(dayKey.Contains("T"));
        }

        [TestMethod]
        public void Test_getWeekKey()
        {
            DateTime dt = new DateTime();
            string weekKey = DateGranulator.getWeekKey(dt);
            Assert.IsTrue(weekKey.Length == 9);
        }

        [TestMethod]
        public void Test_getMonthkKey()
        {
            DateTime dt = new DateTime();
            string monthKey = DateGranulator.getWeekKey(dt);
            Assert.IsTrue(monthKey.Length == 9);
        }


        [DataTestMethod]
        [DataRow("1.1")]
        [DataRow("1,1")]
        [DataRow(".8")]
        public void Test_doubleValidator(string s )
        {
            string ds = DoubleValidator.formatDouble(s).ToString();
            Assert.IsFalse(ds.Contains("."));
            Assert.IsTrue(ds.Contains(","));
        }

    }
}
