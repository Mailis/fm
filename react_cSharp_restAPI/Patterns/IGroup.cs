﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using react_cSharp_restAPI.Models;

namespace react_cSharp_restAPI.Patterns
{
    public interface IGroup
    {
        Dictionary<string, InnerGroup> groupData(List<HourConsumption> hourConsumption);
    }
}
