﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using react_cSharp_restAPI.Models;
using react_cSharp_restAPI.Parsers;

namespace react_cSharp_restAPI.Patterns
{
    public class DayGroup : GeneralGroup, IGroup
    {
        public Dictionary<string, InnerGroup> groupData(List<HourConsumption> hourConsumption)
        {
            if (hourConsumption.Count > 0)
                foreach (HourConsumption h in hourConsumption)
                {
                    if (h != null)
                    {
                        DateTime date = h.date;
                        string key = DateGranulator.getDayKey(date);
                        commonStructure(key, h);
                    }
                }
            return groupDict;
        }
    }
}
