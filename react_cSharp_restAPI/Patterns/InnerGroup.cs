﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace react_cSharp_restAPI.Patterns
{
    [DataContract]
    public class InnerGroup
    {
        [DataMember]
        List<DateTime> dateTimes { get; set; }

        [DataMember]
        double sumOfKwh { get; set; }

        public InnerGroup()
        {
            this.dateTimes = new List<DateTime>();
            this.sumOfKwh = 0;
        }

        public void addDouble(double d)
        {
            this.sumOfKwh += d;
        }

        public void addDateTime(DateTime dt)
        {
            this.dateTimes.Add(dt);
        }

        public void takeEdgesFromDateTimes() {
            if (this.dateTimes.Count() >= 2)
            {
                this.dateTimes.Sort();
                DateTime first = this.dateTimes.First();
                DateTime last = this.dateTimes.Last();
                this.dateTimes.Clear();
                this.dateTimes.Add(first);
                this.dateTimes.Add(last);
            }
        }
    }
}
