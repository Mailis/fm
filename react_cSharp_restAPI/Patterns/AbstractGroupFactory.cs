﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace react_cSharp_restAPI.Patterns
{
    public abstract class AbstractGroupFactory
    {

        public IGroup getObject(string type) // Implementation of Factory Method.
        {
            return this.makeGroup( type);
        }
        protected abstract IGroup makeGroup(string type);
    }
}
