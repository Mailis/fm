﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using react_cSharp_restAPI.Models;

namespace react_cSharp_restAPI.Patterns
{
    public abstract class GeneralGroup
    {
        protected Dictionary<string, InnerGroup> groupDict = new Dictionary<string, InnerGroup>();

        protected void commonStructure(string groupKey, HourConsumption h) {
            InnerGroup innerGroup;
            if (!groupDict.ContainsKey(groupKey))
            {
                innerGroup = new InnerGroup();
                groupDict.Add(groupKey, innerGroup);
            }
            else
            {
                innerGroup = groupDict[groupKey];
            }
            innerGroup.addDateTime(h.date);
            innerGroup.addDouble(h.kwh);
            //store only edge dates inside group
            innerGroup.takeEdgesFromDateTimes();
        }
    }
}
