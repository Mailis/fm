﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace react_cSharp_restAPI.Parsers
{
    public static class DateGranulator
    {
        public static string getDayKey(DateTime date)
        {
            string dy = date.ToString("s").Split('T')[0].Trim();
            return dy;
        }

        public static string getWeekKey(DateTime date)
        {
            string ym = getMonthKey(date); 
            string day = getDayKey(date).Split('-')[2];
            int week_nr = 0;
            try
            {
                int number = Int32.Parse(day);
                week_nr = (number < 7) ? 1 : (number % 7 > 0 ? number/7 +1 : number / 7);
            }
            catch{
            }
            return ym + "-" + week_nr;
        }

        public static string getMonthKey(DateTime date)
        {
            string[] dy = getDayKey(date).Split('-');
            return dy[0] + "-" + dy[1];
        }
    }
}
