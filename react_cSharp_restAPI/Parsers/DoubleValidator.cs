﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace react_cSharp_restAPI.Parsers
{
    public static class DoubleValidator
    {
        public static string formatDouble(string doubleStr)
        {
            if (doubleStr.Contains("."))
            {
                doubleStr = doubleStr.Replace(".", ",");
            }
            return doubleStr;
        }
    }
}
