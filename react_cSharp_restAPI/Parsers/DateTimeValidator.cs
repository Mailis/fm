﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace react_cSharp_restAPI.Parsers
{
    public static class DateTimeValidator
    {
         
        public static bool isValidDate(string date, out DateTime dateValue) {
            bool isParsebleDate = false;
            //try to parse DateTime
            if (DateTime.TryParse(date, result: out dateValue))
            {
                isParsebleDate = true;
            }
            return isParsebleDate;
        }
        public static bool isValidDate(string start, string end, out DateTime startDateValue, out DateTime endDateValue)
        {
            bool isParsebleStartDate1 = isValidDate(start, out startDateValue);
            bool isParsebleStartDate2 = isValidDate(end, out endDateValue);
            return isParsebleStartDate1 && isParsebleStartDate2;
        }

    }
}
