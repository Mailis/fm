import React, { Component } from 'react';
import { Route } from 'react-router';
import { Layout } from './components/Layout';
import { Home } from './components/Home';
import StateValueGetter from './app_identities/state_values'


const hasProps = injectedProps => WrappedComponent => {
    const HasProps = props => <WrappedComponent {...injectedProps} {...props} />
    return HasProps
}

export default class App extends Component {
    displayName = App.name;
    fetchedData = NaN;
    //Avoid unmounted warning
    mounted;

    constructor(props) {
        super(props);
        this.state = {
            startDate: '',
            endDate: '',
            price: 1,
            grouping: 0,
            selected_language: '',
            dataHasChanged: true,
            fechedData: '',
            typeTimeoutID: ''
        }
        this.handleStateChange = this.handleStateChange.bind(this);
        this.saveFetchedData = this.saveFetchedData.bind(this);
        this.ComponentFactory = this.ComponentFactory.bind(this);

        this.mounted = false;
    }

    componentWillUnmount() {
        this.mounted = false;
    }

    componentDidMount() {
        this.mounted = true;
        console.log("this.state APP componentDidMount");
        var stateGetter = new StateValueGetter();
        if (this.mounted) {
            this.setState({
                startDate: stateGetter.start_date,
                endDate: stateGetter.end_date,
                price: stateGetter.price_,
                grouping: stateGetter.group,
                selected_language: stateGetter.language,
                dataHasChanged: true,
                typeTimeoutID: this.props.typeTimeoutID
            });
        }
    }

    saveFetchedData(f_data) {
        console.log("this.state APP saveFetchedData", f_data);
        this.fetchedData = f_data;
    }

    handleStateChange(price, start, end, grouping, selected_language, typeTimeoutID, dataHasChanged = true) {
        if (this.mounted) {
            this.setState({
                startDate: start,
                endDate: end,
                price: price,
                grouping: grouping,
                selected_language: selected_language,
                dataHasChanged: dataHasChanged,
                typeTimeoutID: typeTimeoutID
            });
        }
        //console.log("this.state APP handleStateChange(start)", start);
        //console.log("this.state APP handleStateChange(end)", end);
        //console.log("this.state APP handleStateChange(grouping)", grouping);
        //console.log("this.state APP handleStateChange(price)", price);
        //console.log("this.state APP handleStateChange(selected_language)", selected_language);
    }


    ComponentFactory(componentName) {
        let start = this.state.startDate;
        let end = this.state.endDate;
        let price = this.state.price;
        let grouping = this.state.grouping;
        let dataHasChanged = this.state.dataHasChanged;
        let fetchedData = this.fetchedData;
        let typeTimeoutID = this.state.typeTimeoutID;
        if (componentName.toLowerCase() === "home")
            return hasProps({
                price: price, start: start, end: end, group: grouping, dataHasChanged: dataHasChanged, fetchedData: fetchedData, typeTimeoutID: typeTimeoutID, saveFetchedData : this.saveFetchedData })(Home);
    }


    render() {

    let HomeCompo = this.ComponentFactory("home");

    return (
        <Layout
            handleStateChange={this.handleStateChange}
        >
            <Route exact path='/' component={HomeCompo} />
        </Layout>
    );
  }
}
