﻿import DateStateFactory from '../utils/DateStateFactory'
import data_identity from '../app_identities/app_data_identity'

export default class StateFactory {
    constructor(self, name, value, lang_obj) {
        this.self = self;
        this.name = name;
        this.value = value;
        this.lang_obj = lang_obj;
        this.setConcreteFactory();
    }


    setConcreteFactory() {
        //console.log('err lang_obj', this.lang_obj);
        if (this.name === data_identity.startdate || this.name === data_identity.enddate) {
            this.concreteFactory = new DateStateFactory(this.self, this.name, this.value, this.lang_obj);
        }
    }
    
    validate() {
        let isValidDate = this.concreteFactory.validateState();
        console.log('StateFactory isValidDate', isValidDate);
        return isValidDate;
    };
}




