﻿
export default class StartDateErrorStrategy {
    constructor(self, dateIsInLimits, lang_obj) {
        this.self = self;
        this.dateIsInLimits = dateIsInLimits;
        this.lang_obj = lang_obj;
    }



    setError() {
        let isValidDate = false;
        if (this.dateIsInLimits) {
            this.self.setState({ startDateError: '' });
            isValidDate = true;
        }
        else {
            this.self.setState({ startDateError: this.lang_obj.wrongStartDate });
        }
        return isValidDate;
    };
}