﻿
export default class EndDateErrorStrategy {
    constructor(self, dateIsInLimits, lang_obj) {
        this.self = self;
        this.dateIsInLimits = dateIsInLimits;
        this.lang_obj = lang_obj;
    }


    setError() {
        let isValidDate = false;
        if (this.dateIsInLimits) {
            this.self.setState({ endDateError: '' });
            isValidDate = true;
        }
        else {
            this.self.setState({ endDateError: this.lang_obj.wrongEndDate });
        }
        return isValidDate;
    };
}