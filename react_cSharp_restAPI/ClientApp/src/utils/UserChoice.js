﻿import DateProcessor from '../utils/DateProcessor'

//save user choises into window object
export default class UserChoice {
    static save(key, value) {
        if(key)
            sessionStorage.setItem(key, value);
    }

    static read(key) {
        if (sessionStorage.getItem(key) !== null)
            return sessionStorage.getItem(key);
        return null;
    };
    static readDate(key) {
        if (sessionStorage.getItem(key) !== null)
            return DateProcessor.formatDate(new Date(sessionStorage.getItem(key)));
        return null;
    };

    static remove(key) {
        if (key)
            sessionStorage.removeItem(key);
    };

    static removeAll() {
        sessionStorage.clear();
    };
}




