﻿
export default class DateProcessor{
   

    static getEarliestDate() {
        let date = new Date();
        let twoYearsAgo = new Date(date.setFullYear(date.getFullYear() - 2));
        return twoYearsAgo;
    }

    static getEarliestDateString() {
        return this.formatDate(this.getEarliestDate());
    }

    static formatDate(date) {
        let day = this.addZeroToDate(date.getDate());
        let month = this.addZeroToDate(date.getMonth() + 1);
        let year = this.addZeroToDate(date.getFullYear());
        let formatted = year + '-' + month + '-' + day;
        return formatted;
    }

    static addZeroToDate(nr) {
        let date_nr = parseInt(nr, 10);
        return date_nr < 10 ? "0" + nr : nr;
    }
}

