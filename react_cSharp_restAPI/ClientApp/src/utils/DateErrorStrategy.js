﻿export default class DateErrorStrategy {
    constructor(self, concreteDateErrorStrategy, isStartSmallerOrWqualToEnd, lang_obj) {
        this.self = self;
        this.concreteDateErrorStrategy = concreteDateErrorStrategy;
        this.isStartSmallerOrWqualToEnd = isStartSmallerOrWqualToEnd;
        this.lang_obj = lang_obj;
    }


    setErrors() {
        let hasNotError = true;
        if (this.isStartSmallerOrWqualToEnd) {
            //console.log('self.setState: ------ start is before end or equal');
            this.self.setState({ endDateIsBeforeStartDateError: '' });
        }
        else {
            //console.log('self.setState: ------ start is NOT before end ');
            this.self.setState({ endDateIsBeforeStartDateError: this.lang_obj.startDateIsSmallerThanEndDate });
            hasNotError = false;
        }
        //console.log('self.state', this.self.state);
        let concreteDateIsValid = this.concreteDateErrorStrategy.setError();
        return hasNotError && concreteDateIsValid;
    }
}