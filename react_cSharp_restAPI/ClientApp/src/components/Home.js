import React, { Component } from 'react';
import AccessToContr from '../controllers/accessToController'
import LanguageFactory from '../i18n/LanguageFactory'
import '../css/home.css';

export class Home extends Component {
    displayName = Home.name;
    LanguageFactory = new LanguageFactory(this, NaN);
    //Avoid unmounted warning
    mounted;

    constructor(props) {
        super(props);
        this.state = {
            fetched_KWH: this.props.fetchedData
        }
        this.fetchData = this.fetchData.bind(this);
        this.displayData = this.displayData.bind(this);
        this.makeRows = this.makeRows.bind(this);

        this.mounted = false;
    }


    componentWillUnmount() {
        this.mounted = false;
    }

    componentWillMount() {
        console.log("HOME componentWillMount");

        let start = this.props.start;
        let end = this.props.end;
        let group = this.props.group;
        
        let data_was_changed = this.props.dataHasChanged;
        console.log("HOME data_was_changed: ", data_was_changed);
        if (data_was_changed) {
            this.fetchData(start, end, group);
        }
        else {
            let prev_fetchedData = this.props.fetchedData;
            console.log("HOME fetchedData", prev_fetchedData);
            this.setState({
                fetched_KWH: prev_fetchedData
            })
        }
    }

    componentDidMount() {
        this.mounted = true;
        //console.log("HOME componentDidMount");
        this.LanguageFactory.setLanguage(this.props.language);
        window.clearTimeout(this.props.typeTimeoutID);
    }

    displayData(fetched_data) {
       
        if (typeof fetched_data === typeof ''){//error
            return fetched_data;
        }
        let rows = this.makeRows(fetched_data);
        return (
            <div className='table'>
                    {rows.map((row, i) => 
                    <div key={i}>
                            <div>{row[i]}</div>
                        </div>
                    )}
            </div>
        );
    }


    makeRows(fetched_data) {
        let lang_data = this.LanguageFactory.langObject;
        if (typeof fetched_data === typeof '') {//error
            return fetched_data;
        }

        let rows = [];
        let innerRows = [];
        
        let group_type = this.LanguageFactory.langObject.select_grouping[this.props.group];
        innerRows.push(
            <div className="cellrow" key={"head"}>
                <div className="cell head">{group_type}</div>
                <div className="cell head">{lang_data.period}</div>
                <div className="cell head">kWh</div>
                <div className="cell head">{lang_data.price}</div>
                <div className="clear"></div>
            </div>
        );
        let k_count = 0;
        for (var key in fetched_data) {
            k_count++;
            if (fetched_data.hasOwnProperty(key)) {
                let data = fetched_data[key];
                let datetimes = "";
                if (data["dateTimes"]) {
                    data["dateTimes"].map((dt, j) =>
                        datetimes += (j > 0) ? (dt.split("T")[0]) : (dt.split("T")[0] + " - " )
                    )
                }
                let kWh = Math.round(parseFloat(data["sumOfKwh"]) * 100) / 100;
                let price = Math.round(kWh * parseInt(this.props.price) * 100) / 100;
                innerRows.push(
                    <div className="cellrow" key={k_count}>
                        <div className="cell">{key}</div>
                        <div className="cell">{datetimes}</div>
                        <div className="cell">{kWh}</div>
                        <div className="cell">{price}</div>
                        <div className="clear"></div>
                    </div>
                );
                rows.push(innerRows);
            }
        }
        return rows;
    }

    fetchData(start, end, group) {

        //console.log("HOME data_was_changed fetchData ");
        if (start && end) {
            var a = new AccessToContr(this, start, end, group);
            a.fetchData();
        }
    }

    render() {

        console.log("HOME render this.props", this.props);
        //console.log("HOME  this.props.price:", price);
        //console.log("HOME  this.props.start:", start);
        //console.log("HOME  this.props.end:", end);
        //console.log("HOME  this.props.group:", group);

        const f = this.displayData(this.state.fetched_KWH);
        //console.log("HOME STATE: this.mounted", f );
        let lang_data = this.LanguageFactory.langObject;
        const price = lang_data.price + ": " + this.props.price;
        const heading = lang_data.electricity_consumption_computing;
        
      return (
        
          <div>
              <h1>{heading}</h1>
              <h1>{price}</h1>
                <br />
                  {f}
          </div>
    );
  }
}
