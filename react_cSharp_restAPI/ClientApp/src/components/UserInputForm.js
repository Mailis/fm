﻿import React, { Component } from 'react';
import StateFactory from '../utils/StateFactory';
import UserChoice from '../utils/UserChoice';
import app_data_identity from '../app_identities/app_data_identity';
import LanguageFactory from '../i18n/LanguageFactory';
import StateValueGetter from '../app_identities/state_values';
import '../css/user_form.css';
import DateProcessor from '../utils/DateProcessor';

export class UserInputForm extends Component {
    displayName = UserInputForm.name;

    constructor(props) {
        super(props);
        this.state = {
            startDate: '',
            endDate: '',
            price: 1,
            grouping: 0,
            endDateIsBeforeStartDateError: '',
            startDateError: '',
            endDateError: '',
            fetched_KWH: '',
            selected_language: ''
        };
        this.startDatePickerChanged = this.startDatePickerChanged.bind(this);
        this.endDatePickerChanged = this.endDatePickerChanged.bind(this);
        this.getGroupOptionsList = this.getGroupOptionsList.bind(this);
        this.priceChanged = this.priceChanged.bind(this);
        this.groupChanged = this.groupChanged.bind(this);
        this.languageChanged = this.languageChanged.bind(this);
        this.getLanguageButtons = this.getLanguageButtons.bind(this);

        this.LanguageFactory = new LanguageFactory(this, NaN);
        this.stateGetter = new StateValueGetter();
        this.dateTimeInputDelay = 3000;
        this.typeTimeoutID = NaN;
    }

    componentWillMount() {
        this.setState({
            startDate: this.stateGetter.start_date,
            endDate: this.stateGetter.end_date,
            price: this.stateGetter.price_,
            grouping: this.stateGetter.group,
            selected_language: this.LanguageFactory.getLanguage()
        });
        window.clearTimeout(this.typeTimeoutID);
    }

    languageChanged(evt) {
        if (evt) {
            evt.preventDefault();
            //console.log('getLanguageButtons evt.target', evt.target);
            let name = evt.target.value;
            if (name) {
                //console.log('languageChanged target.name', name);
                this.setState({
                    selected_language: name
                });
                UserChoice.save(app_data_identity.language, name.toUpperCase());
                this.LanguageFactory.setLanguage(name);
            }

            this.props.handleStateChange(this.state.price, this.state.startDate, this.state.endDate, this.state.grouping, name, this.typeTimeoutID, false);
        }
    }

    startDatePickerChanged(evt) {
        //validate datetime input
        let isValue = this.datePickerChanged(evt);
        if (isValue !== 0) {
            //console.log('typeTimeoutID enter timer');
            if (this.typeTimeoutID) {
                //console.log('typeTimeoutID exists');
                window.clearTimeout(this.typeTimeoutID);
            }
            //wait for user has finished typing a datetime
            this.typeTimeoutID = window.setTimeout(this.notifyChangedDate, this.dateTimeInputDelay, isValue, true);
        }
    }

    endDatePickerChanged(evt) {
        //validate datetime input
        let isValue = this.datePickerChanged(evt);
        if (isValue !== 0) {
            //console.log('typeTimeoutID enter timer');
            if (this.typeTimeoutID) {
                //console.log('typeTimeoutID exists');
                window.clearTimeout(this.typeTimeoutID);
            }
            //wait for user has finished typing a datetime
            this.typeTimeoutID = window.setTimeout(this.notifyChangedDate, this.dateTimeInputDelay, isValue, false);
        }
    }

    /**
    * wait for user has finished typing a datetime
    * @param {DateTime} value value, unformatted
    * @param {boolean} isStart wether it is start or end date
    **/
    notifyChangedDate = (value, isStart) => {
        //console.log('typeTimeoutID notifyChangedDate value', value, isStart);
        let d = DateProcessor.formatDate(value);
        if (isStart)
            this.props.handleStateChange(this.state.price, d, this.state.endDate, this.state.grouping, this.state.selected_language, this.typeTimeoutID, true);
        else
            this.props.handleStateChange(this.state.price, this.state.startDate, d, this.state.grouping, this.state.selected_language, this.typeTimeoutID, true);

        window.clearTimeout(this.typeTimeoutID);
        //console.log('typeTimeoutID exited timer');
    }


    datePickerChanged = (evt) => {
        let isValue = 0;
        if (evt) {
            evt.preventDefault();
            let name = evt.target.name;
            let value = new Date(evt.target.value);
            let lang_obj = this.LanguageFactory.getLangObject();
            let stateFactory = new StateFactory(this, name, value, lang_obj);
            let isValidDate = stateFactory.validate();
            if (isValidDate) {
                isValue = value;
            }
        }
        return isValue;
    }

    priceChanged(evt) {
        //console.log('priceChanged');
        if (evt) {
            evt.preventDefault();
            window.clearTimeout(this.typeTimeoutID);
            let value = parseFloat(evt.target.value);
            this.setState({ price: value });
            UserChoice.save(app_data_identity.price, value);
            this.props.handleStateChange(value, this.state.startDate, this.state.endDate, this.state.grouping, this.state.selected_language, this.typeTimeoutID, false );

        }
    }

    groupChanged(evt) {
        //console.log('groupChanged');
        if (evt) {
            evt.preventDefault();
            window.clearTimeout(this.typeTimeoutID);
            let value = parseInt(evt.target.value, 10);
            //console.log('evt.target', evt.target);
            //console.log('evt.target.value', evt.target.value);
            this.setState({ grouping: value });
            UserChoice.save(app_data_identity.group, value);
            this.props.handleStateChange(this.state.price, this.state.startDate, this.state.endDate, value, this.state.selected_language, this.typeTimeoutID, true);
        }
    }

    getGroupOptionsList() {
        let grouping_options = this.LanguageFactory.langObject.select_grouping;
        let opt_rows = [];
        let counter = 0;

        for (let opt of grouping_options) {
            opt_rows.push(<option key={counter} value={counter} defaultValue>{opt}</option>);
            counter++;
        }
        return opt_rows;
    }


    getLanguageButtons() {
        let opt_rows = [];
        let counter = 0;

        let language_options = app_data_identity.languages;
        for (let opt of language_options) {
            opt_rows.push(<option key={counter} name={opt} value={opt} defaultValue>{opt}</option>);
            counter++;
        }
        return opt_rows;
    }

    render() {
        //language buttons (select drop down list)
        let lang_selection = this.getLanguageButtons();
        //user interactions (Input values)
        let startDate = this.state.startDate;
        //console.log('startDate', startDate);
        let endDate = this.state.endDate;
        //console.log('endDate', endDate);
        let opt_rows = this.getGroupOptionsList();
        this.LanguageFactory.setLanguage(this.state.selected_language);
        let lang_data = this.LanguageFactory.langObject;

        //console.log("USERINPUT selected_language ", this.state.selected_language);

        return (
            <div className="user_form_container">
                <fieldset>
                    <div className="user_input">
                        <select
                            value={this.state.selected_language}
                            onChange={this.languageChanged}
                        >
                            {lang_selection}
                        </select>
                    </div>
                </fieldset>
                <fieldset>
                    <legend>{lang_data.select_dates_guide + ":"}</legend>
                    <div className="date_picker_error">{this.state.endDateIsBeforeStartDateError}</div>

                    <div className="date_picker_guide">{lang_data.select_dates_startdate + ":"}</div>
                    <div className="user_input">
                        <input
                            type="date"
                            id="start"
                            placeholder={lang_data.select_dates_startdate}
                            name={app_data_identity.startdate}
                            onChange={this.startDatePickerChanged}
                            value= {startDate}
                        />
                    </div>
                    <div className="date_picker_error">{this.state.startDateError}</div>

                    <div className="date_picker_guide">{lang_data.select_dates_enddate + ":"}</div>
                    <div className="user_input">
                        <input
                            type="date"
                            id="end"
                            placeholder={lang_data.select_dates_enddate}
                            name={app_data_identity.enddate}
                            onChange={this.endDatePickerChanged}
                            value={endDate}
                        />
                    </div>
                    <div className="date_picker_error">{this.state.endDateError}</div>
                </fieldset>
                <fieldset>
                    <legend>{lang_data.select_grouping_guide + ":"}</legend>
                    <div className="user_input">
                        <select
                            value={this.state.grouping} 
                            onChange={this.groupChanged}
                        >
                            {opt_rows}
                        </select>
                    </div>
                </fieldset>
                <fieldset>
                    <legend>{lang_data.select_price_guide + ":"}</legend>
                    <div className="user_input">
                        <input
                            type="number"
                            id="price"
                            name={app_data_identity.price}
                            onChange={this.priceChanged}
                            value={this.state.price}
                            min="0"
                        />
                    </div>
                </fieldset>
            </div>
        );
    }
}
