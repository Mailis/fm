﻿import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Glyphicon, Nav, Navbar, NavItem } from 'react-bootstrap';
import { LinkContainer } from 'react-router-bootstrap';
import { UserInputForm } from './UserInputForm';
import LanguageFactory from '../i18n/LanguageFactory'
import '../css/NavMenu.css';


export class NavMenu extends Component {
  displayName = NavMenu.name


  getLanguage = () => {
      let langFactory = new LanguageFactory(this, this.props.language);
      return langFactory.getLangObject();
  }

  render() {
    let lang_data = this.getLanguage();

    return (
      <Navbar inverse fixedTop fluid collapseOnSelect>
        <Navbar.Header>
          <Navbar.Brand>
                    <Link to={'/'}>
                        <span className="navbar_brand">
                            KW/h
                        </span>
                    </Link>
          </Navbar.Brand>
          <Navbar.Toggle />
        </Navbar.Header>
        <UserInputForm
                handleStateChange={this.props.handleStateChange}
         />
        <Navbar.Collapse>
          <Nav>
            <LinkContainer to={'/'} exact>
              <NavItem >
                <Glyphicon glyph='th-list' /> {lang_data.nav_table_view}
              </NavItem>
            </LinkContainer>
          </Nav>
        </Navbar.Collapse>

        <div className="author">site design / development</div>
        <div className="author">© Mailis Toompuu</div>
        <div className="author">2018</div>
        
        </Navbar>

    );
  }
}
