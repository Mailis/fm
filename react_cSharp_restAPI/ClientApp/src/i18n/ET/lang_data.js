﻿const app_data = {
    price: "hind",
    period: "periood",
    select_dates_guide: "Vali algus- ja lõppkuupäev",
    select_dates_startdate: "ALGUSKUUPÄEV",
    select_dates_enddate: "LÕPPKUUPÄEV",
    select_grouping_guide: "Vali grupp",
    select_grouping: ['päev', 'nädal', 'kuu'],
    select_price_guide: "Vali kW/h hind",

    wrongStartDate: "Alguskuupäev ei tohi olla varasem, kui kaks aastat tagasi ega tulevikus.",
    wrongEndDate: "Lõppkuupäev ei tohi olla tulevikus ega varasem, kui kaks aastat tagasi.",
    startDateIsSmallerThanEndDate: "Alguskuupäev peab olema varasem või sama kui lõppkuupäev.",

    electricity_consumption_computing: "Elektrikulu arvutamine",

    nav_table_view: "Tabeli vaade",
}

export default app_data;