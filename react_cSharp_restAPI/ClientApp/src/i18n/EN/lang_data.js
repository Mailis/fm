﻿const app_data = {
    price: "price",
    period: "period",
    select_dates_guide: "Select start and end date",
    select_dates_startdate: "START DATE",
    select_dates_enddate: "END DATE",
    select_grouping_guide: "Select grouping",
    select_grouping: ['day', 'week', 'month'],
    select_price_guide: "Set price for kW/h",

    wrongStartDate: "Start date must be at most two years ago and not in the future.",
    wrongEndDate: "End date must be at most two years ago and not in the future.",
    startDateIsSmallerThanEndDate: "Start date must become before end date or be the same.",

    electricity_consumption_computing: "Computing electricity consumption",

    nav_table_view: "Table view",
}

export default app_data;