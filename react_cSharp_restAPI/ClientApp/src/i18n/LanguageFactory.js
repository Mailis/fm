﻿import lang_data_ET from '../i18n/ET/lang_data'
import lang_data_EN from '../i18n/EN/lang_data'
import app_data_identity from '../app_identities/app_data_identity'
import StateValueGetter from '../app_identities/state_values'

export default class LanguageFactory {
    constructor(self, language) {
        this.self = self;

        if (language) {
            this.language = language;
        }
        else {
            let stateGetter = new StateValueGetter();
            this.language = stateGetter.language || app_data_identity.languages[0] || "ET";
        }
        this.langObject = NaN;
        this.setLangObject();
    }

    getLanguage() {
        return this.language;
    }

    setLanguage(language) {
        this.language = language;
        this.setLangObject();
    }

    setLangObject() {
        if (this.language) {
            let upperCode = this.language.toUpperCase();
            //console.log('this.language', this.language);
            if (upperCode.includes("ET")) 
                this.langObject = lang_data_ET;
            else if (upperCode.includes("EN")) 
                this.langObject = lang_data_EN;
            else
                this.langObject = lang_data_EN;
            //console.log('upperCode', upperCode);
            //console.log('upperCode.includes("ET")', upperCode.includes("ET"));
            //console.log('upperCode.includes("EN")', upperCode.includes("EN"));
        }
        //console.log('upperCode langObject', this.langObject);
        return this.langObject;
    }

    getLangObject() {
        return this.langObject;
    }
}