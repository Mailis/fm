﻿const app_identities = {
    startdate: "start_date",
    enddate: "end_date",
    price: "price",
    group: "group",
    grouping: ['day', 'week', 'month'],
    language: "language",
    languages: ['ET', 'EN'],
    
}

export default app_identities;