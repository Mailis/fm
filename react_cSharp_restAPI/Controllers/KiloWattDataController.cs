﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Xml;
using System.Xml.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using react_cSharp_restAPI.Caching;
using react_cSharp_restAPI.Models;
using react_cSharp_restAPI.Parsers;
using react_cSharp_restAPI.Patterns;

namespace react_cSharp_restAPI.Controllers
{
    [Produces("application/json")]
    [Microsoft.AspNetCore.Mvc.Route("api/KiloWattData")]
    public class KiloWattDataController : Controller
    {
        private IMemoryCache cache;
        MemoryCacheEntryOptions cacheExpirationOptions;
        int cacheDays = 1;
        string errorMessage = "Error: invalid data! \n ";

        //inject caching interface
        public KiloWattDataController(IMemoryCache cache)
        {
            this.cache = cache;
            //set cachexpire options
            cacheExpirationOptions = new MemoryCacheEntryOptions();
            cacheExpirationOptions.AbsoluteExpiration = DateTime.Now.AddDays(cacheDays);
            cacheExpirationOptions.Priority = CacheItemPriority.Normal;
        }

        // GET: api/KiloWattData/5/6
        [Microsoft.AspNetCore.Mvc.HttpGet("{start}/{end}/{group_index}/{grouparray}", Name = "Get")]
        public IDictionary<string, InnerGroup> Get(string start, string end, int group_index, string grouparray)
        {
            string[] arr = grouparray.Split(':');
            group_index = group_index > arr.Length ? arr.Length - 1 : (group_index < 0 ? 0 : group_index);
            //get group type string
            string grp = arr[group_index];

            //parsed dates
            DateTime startDateTime, endDateTime;
            //are date time strings parseble
            if (!DateTimeValidator.isValidDate(start, end, out startDateTime, out endDateTime))
            {
                HttpResponseMessage message = new HttpResponseMessage(HttpStatusCode.BadRequest);
                message.Content = new StringContent("Start or End date have bad formatting.");
                throw new HttpResponseException(message);
            }

            //look for cached data
            string casheKey = CacheKeyCreator.getCacheKey(start, end, grp);
            string obj;
            if (cache.TryGetValue(casheKey, out obj))
            {
                return JsonConvert.DeserializeObject<IDictionary<string, InnerGroup>>(obj);
            }
            


            //construct request url
            string url = "https://finestmedia.ee/kwh/?start=" + start + "&end=" + end;

            //fetch remote data
            WebClient client = new WebClient();
            client.Headers["Accept"] = "application/xml";
            //returned data
            string returnedString = client.DownloadString(new Uri(url));
            try
            {
                //parse hour consumption from XML data
                XElement root = XElement.Parse(returnedString);

                //collect data into HourConsumptionBeforeParse
                //in order to try parse its values later
                var q = from history in root.Descendants("ConsumptionHistory")
                        from element in history.Elements()
                        select new HourConsumptionBeforeParse(element.Attribute("ts").Value, element.Value.Replace(".", ","));

                RawDataParser rawParser = new RawDataParser(startDateTime, endDateTime, q);
                //stores parsed data
                List<HourConsumption> hourConsumption = rawParser.parseRawData();
                //factory pattern for producing a group by group name
                AbstractGroupFactory groupFactory = new ConcreteGroupFactory();
                IGroup concreteGroup = groupFactory.getObject(grp);
                //string is a number of  month, week or day of a year
                //innergroup sums up KWH values and stores dates of a period for a group
                IDictionary<string, InnerGroup> groupedData = concreteGroup.groupData(hourConsumption);
                //cache the data
                cache.Set<string>(casheKey, JsonConvert.SerializeObject(groupedData), cacheExpirationOptions);
                return groupedData;
            }
            catch (XmlException e)
            {
                if (returnedString.Length < 100)//response is not xml, it is a messsage like "Invalid data"
                {
                    HttpResponseMessage message = new HttpResponseMessage(HttpStatusCode.NotFound);
                    message.Content = new StringContent(returnedString);
                    throw new HttpResponseException(message);
                }
                throw new XmlException(errorMessage, e);
            }
            catch (Exception e)
            {
                throw new Exception(errorMessage, e);
            }

        }

    }
}
