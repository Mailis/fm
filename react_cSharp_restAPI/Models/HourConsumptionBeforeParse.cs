﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace react_cSharp_restAPI.Models
{
    public class HourConsumptionBeforeParse
    {
        public string date = "";
        public string kwh = "";
        public HourConsumptionBeforeParse(string date, string kwh)
        {
            this.date = date;
            this.kwh = kwh;
        }
    }
}
