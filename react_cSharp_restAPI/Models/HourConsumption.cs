﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace react_cSharp_restAPI.Models
{
    public class HourConsumption
    {
        public DateTime date {get; private set;}
        public double kwh {get; private set;}
        public HourConsumption(DateTime date, double kwh)
            {
                this.date = date;
                this.kwh = kwh;
            }
        }
}
