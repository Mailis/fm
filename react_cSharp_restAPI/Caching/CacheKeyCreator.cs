﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace react_cSharp_restAPI.Caching
{
    public static class CacheKeyCreator
    {
        public static string getCacheKey(string start, string end, string group)
        {
            return start + "_" + end + "_" + group;
        }
    }
}
